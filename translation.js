var resources = {
	"en": {
		translation: {
			"Select" : "Select",
			"Plow" : "Plow",
			"Seed" : "Seed",
			"Harvest" : "Harvest",
			"Water" : "Water",
			"Selects a cell to inspect" : "Selects a cell to inspect",
			"Working Power" : "Working Power"
		}
	},
	"ja": {
		translation: {
			"Select" : "選択",
			"Plow" : "耕す",
			"Seed" : "種まき",
			"Tuber" : "種芋",
			"Harvest" : "収穫",
			"Water" : "水やり",
			"Weed" : "除草",
			"Mulch" : "マルチ",
			"Fertilize" : "施肥",
			"Selects a cell to inspect" : "セルを選択して調査",
			"Plow and make ridges" : "耕して畝を作る",
			"Apply crop seeds" : "作物の種を植える",
			"Plant seed tubers of potatos" : "種芋を植える",
			"Harvest and sell crops\nto gain money" : "作物を収穫して販売する",
			"Water soil" : "土壌に水を撒く",
			"Weed out without plowing\nSoil humidity is rather kept" : "耕さずに雑草を引き抜く\n土壌の水分は比較的維持される",
			"Mulch soil with plastic blanket\nKeeps soil humidity" : "プラスチックのマルチシートで覆う\n土壌の水分を維持する",
			"Add organic fertilizer to soil\nHelps crops grow" : "有機堆肥を土壌に追加する\n作物が育つのを助ける",
			"Working Power" : "労働力",
			"Working Power Cost" : "労働力消費",
			"Cash" : "資金",
			"Weather" : "天気",
			"Sunny" : "晴れ",
			"Partly cloudy" : "晴れ時々曇り",
			"Cloudy" : "曇り",
			"Rainy" : "雨",
			"Money Cost" : "費用",
			"Pos" : "位置",
			"Weeds" : "雑草",
			"Plowed" : "畝立て",
			"Fertility" : "肥沃度",
			"Corn" : "トウモロコシ",
			"Potato" : "ジャガイモ",
			"Potato Pest" : "イモ疫病",
			"growth" : "生育",
			"quality" : "品質",
			"value" : "販価",
			"Humidity" : "湿度"
		},
		buttonFont : {
			"Corn" : "10px MS Gothic",
			"Potato" : "10px MS Gothic"
		}
	}
}
